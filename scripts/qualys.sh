#! /bin/bash

# environment variables

# ARN    = "${aws_iam_role.qualys.arn}"
# EID    = "${random_string.externalid.result}"
# NAME   = "${var.name}"
# DESC   = "${var.description}"
# Q_USER = "${var.user}"
# Q_PASS = "${var.secret}"
# URL    = "${var.url}"

# Exit if any of the intermediate steps fail

OUT="/tmp/$$-out.json"
IN="/tmp/$$-in.json"
qualys_ready=0

function cleanup () {
  [ -f "${OUT}" ] && rm -f "${OUT}"
  [ -f "${IN}" ]  && rm -f "${IN}"
}



function qualys_api () {
  q_url="$1"
  out="$2"
  in="$3"
  method="$4"
  in_param=()

  [ -f "${in}" ] && in="@${in}"
  
  [ -n "${in}" ] && in_param=(-d "${in}")
  
  if [ -z "${method}" ]; then
    if [ -n "${in}" ]; then
      method=POST
    else
      method=GET
    fi
  fi

  /usr/bin/curl -X "${method}" \
                -H 'Accept: application/json' \
                -u "$Q_USER:$Q_PASS" \
                "${in_param[@]}" \
                -o "${out}" \
                "${q_url}" \

  error=$(jq -r '.error' "${OUT}")

  if [ -n "${error}" ] && [ "${error}" != null ]; then
    echo ERROR: "${error}" >&2
    exit 1
  fi
  
}

# Qualys seems to not accept new connector requests unless a connect list
# is performed first
#
# TODO: 2/17/2021 - this may not be needed anymore

function qualys_test_ready () {
  attempts=0
  qualys_ready=0

  while [ "$attempts" -lt 20 ]; do
    attempts=$((attempts+1))
    qualys_api "${URL}/?pageSize=5000" "${OUT}"
    
    if [ -s "${OUT}" ] ; then
      # output was not empty
      status=$(jq .status "${OUT}")
      errorcode=$(jq .errorCode "${OUT}")
      length=$(jq '.content|length' "${OUT}")
      if [ "${status}" != "401" ] && [ "${errorcode}" != '"401"' ] && [ "${length}" != "0" ]; then
        # shellcheck disable=SC2034
        qualys_ready=1
        break;
      fi
    fi
  done
}

trap cleanup EXIT INT QUIT TERM

