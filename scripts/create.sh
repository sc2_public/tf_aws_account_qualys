#! /bin/bash

# get parameters

# ARN    = "${aws_iam_role.qualys.arn}"
# EID    = "${random_string.externalid.result}"
# NAME   = "${var.name}"
# DESC   = "${var.description}"
# Q_USER = "${var.user}"
# Q_PASS = "${var.secret}"
# URL    = "${var.url}"

# shellcheck source=qualys.sh
. $(dirname "${0}")/qualys.sh

jq -n --arg arn "$ARN" --arg eid "$EID" --arg name "$NAME" --arg desc "$DESC" '{
    arn:         "$ARN",
    externalId:  "$EID",
    name:        "$NAME",
    description: "$DESC"
}' >"${IN}"

qualys_api "${URL}" "${OUT}" "${IN}"

cat "${OUT}"

exit 0
