#! /bin/bash

set -e

# get parameters

# ARN    = "${aws_iam_role.qualys.arn}"
# EID    = "${random_string.externalid.result}"
# NAME   = "${var.name}"
# DESC   = "${var.description}"
# Q_USER = "${var.user}"
# Q_PASS = "${var.secret}"
# URL    = "${var.url}"
# ID     = "${data.aws_caller_identity.self.account_id}"

# shellcheck source=qualys.sh
. $(dirname "${0}")/qualys.sh

# read JSON input and extract connectorId
connector_id=$(jq -r .connectorId)

qualys_api "${URL}"/"${connector_id}" "${OUT}"

cat "${OUT}"

exit 0


