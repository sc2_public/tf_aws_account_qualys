#
# Create policies and role for Qualys CloudView access
#

terraform {
  required_providers {
    shell = {
      source  = "scottwinkler/shell"
      version = "~>1.7.7"
    }
  }
}  

# generate a random string for the externalID
resource "random_string" "externalid" {
  length  = 13
  number  = true
  upper   = false
  lower   = false
  special = false
}

data "aws_iam_policy_document" "qualys" {
  statement {

    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "AWS"
      identifiers = [ "arn:aws:iam::${var.account}:root" ]
    }

    condition {
      test = "StringEquals"
      variable = "sts:ExternalId"
      values = [ random_string.externalid.result ]
    }
  }
}

resource "aws_iam_role" "qualys" {
  name               = "qualys-cloud-view"
  assume_role_policy = data.aws_iam_policy_document.qualys.json
}

resource "aws_iam_role_policy_attachment" "qualys_audit" {
  role       = aws_iam_role.qualys.name
  policy_arn = "arn:aws:iam::aws:policy/SecurityAudit"
}

data "aws_iam_policy_document" "s3_publicaccess_check" {
  statement {
    actions = [
      "s3:GetAccountPublicAccessBlock",
      "s3:GetBucketPublicAccessBlock"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "s3_publicaccess_check" {
  name   = "s3-publicaccess-check"
  path   = "/"
  policy = data.aws_iam_policy_document.s3_publicaccess_check.json
}

resource "aws_iam_role_policy_attachment" "qualys_s3" {
  role       = aws_iam_role.qualys.name
  policy_arn = aws_iam_policy.s3_publicaccess_check.arn
}

provider "shell" {
  sensitive_environment = {
    Q_PASS = var.secret
  }
}

resource "shell_script" "qualys_cloudview" {
  lifecycle_commands {
    create = file("${path.module}/scripts/create.sh")
    read   = file("${path.module}/scripts/read.sh")
    delete = file("${path.module}/scripts/delete.sh")

    # No update - as of 03/23/2021 we cannot think of a need for
    # updating an existing Qualys integration.  If we start allowing
    # accounts to be renamed, we might need update here

    # update = file("${path.module}/scripts/update.sh")

  }

  environment = {
    ARN    = aws_iam_role.qualys.arn
    ID     = data.aws_caller_identity.self.account_id
    EID    = random_string.externalid.result
    NAME   = var.name
    DESC   = var.description
    Q_USER = var.user
    URL    = var.url
  }
}


