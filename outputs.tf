output "external_id" {
  value = data.external.connector.result.externalId
}

output "connector_id" {
  value = data.external.connector.result.connectorId
}

output "error" {
  value = data.external.connector.result.error
}
