#! /bin/bash -x

# Exit if any of the intermediate steps fail
set -e

# get parameters

# ARN  = "${aws_iam_role.qualys.arn}"
# EID  = "${random_string.externalid.result}"
# NAME = "${var.name}"
# DESC = "${var.description}"
# USER = "${var.user}"
# PASS = "${var.secret}"
# URL  = "${var.url}"


eval "$(jq -r '@sh "ARN=\(.arn) ID=\(.id) EID=\(.eid) NAME=\(.name) DESC=\(.desc) USER=\(.user) PASS=\(.pass) URL=\(.url)"')"

# Qualys seems to not accept new connector requests unless a connect list
# is performed first

attempts=0
success=0

while [ $attempts -lt 20 ]; do
  attempts=$((attempts+1))
  /usr/bin/curl -H 'Accept: application/json' -u $USER:$PASS -sS $URL/?pageSize=5000 -o /tmp/${EID}-accounts.json

  if [ -s /tmp/${EID}-accounts.json ] ; then
    # /tmp/${EID}-accounts.json was not empty
    status=$(jq .status /tmp/${EID}-accounts.json)
    errorcode=$(jq .errorCode /tmp/${EID}-accounts.json)
    length=$(jq '.content|length' /tmp/${EID}-accounts.json)
    if [ "${status}" != "401" ] && [ "${errorcode}" != '"401"' ] && [ "${length}" != "0" ]; then
      success=1
      break;
    fi
  fi
done

if [ $success -eq 0 ]; then
  echo ERROR: Failed to get list of accounts from Qualys >&2
  exit 1
fi

jq --arg eid "$EID" --arg id "$ID"  '.content[]|select(.externalId==$eid and .awsAccountId==$id)' /tmp/${EID}-accounts.json > /tmp/${EID}.json

if [ ! -s /tmp/${EID}.json ]; then
  # JSON file is empty, so account needs to be registered

  jq -n --arg arn "$ARN" --arg eid "$EID" --arg name "$NAME" --arg desc "$DESC" '{
    arn:         $arn,
    externalId:  $eid,
    name:        $name,
    description: $desc
  }' >/tmp/${EID}-in.json

  /usr/bin/curl -u $USER:$PASS -sS \
                -H 'Content-Type: application/json' \
                -d @/tmp/${EID}-in.json \
                -o /tmp/${EID}.json \
                $URL

  eval $(jq -r '@sh "ERROR=\(.error)"' /tmp/${EID}.json)

  if [ -n "${ERROR}" ]; then
    echo ERROR: ${ERROR} >&2
    exit 1
  fi

fi

jq '{connectorId,externalId}' /tmp/${EID}.json
exit 0

