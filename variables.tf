/*
 * variables for creating Qualys CloudView connectors
 */

data "aws_caller_identity" "self" {}

variable "name" {
  description = "Name for Qualys CloudView connector"
  type        = string
}

variable "url" {
  description = "Qualys CloudView Connectors API endpoint"
  type        = string
  default     = "https://qualysguard.qg2.apps.qualys.com/cloudview-api/rest/v1/aws/connectors"
}

variable "account" {
  description = "Qualys CloudView API AWS Account ID"
  type        = string
  default     = "805950163170"
}

variable "user" {
  type        = string
  description = "Qualys CloudView API user"
  default     = "sunet2sa5"
}

variable "secret" {
  type        = string
  description = "Qualys CloudView API secret"
}

variable "description" {
  description = "Description for Qualys CloudView API call"
  type        = string
  default     = "Created by Terraform"
}
