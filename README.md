# tf_aws_account_qualys

Links an account to Qualys CloudView

* Creates a role for Qualys, with a random external ID
* Registers the account, role, and external ID via the Qualys CloudView API


## Variables

| Name          | Type   | Description                      | Default                                                                      |
|---------------|--------|----------------------------------|------------------------------------------------------------------------------|
| `name`        | string | Name for CloudView connector     | _none_                                                                       |
| `url`         | string | Qualys API endpoint              | https://qualysguard.qg2.apps.qualys.com/cloudview-api/rest/v1/aws/connectors |
| `account`     | string | Qualys CloudView Account ID      | 805950163170                                                                 |
| `user`        | string | Qualys API user                  | _see code_                                                                   |
| `secret`      | string | Qualys API secret                | _none_                                                                       |
| `description` | string | Description for CloudView entity | "Created by Terraform"                                                       |


## Outputs

| Name          | Type   | Description                             |
|---------------|--------|-----------------------------------------|
| `external_id` | string | External ID created for role assumption |
| `conector_id` | string | Qualys CloudView Connector ID           |
| `error`       | string | Error message if API call failed        |


## Usage

This module should only be instantiated once per account, and will run in the default region.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "foo-prod"
}

module "qualys" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_qualys.git"
  name   = "test37"
  secret = var.qualys_secret
}
```

`secret` should never be written to a file; instead use:

```bash
terraform apply -var qualys_secret=XXXXX
```

## Related Modules

* [AWS Default Setup](https://code.stanford.edu/sc2_public/tf_aws_account_default)
* [AWS Config setup](https://code.stanford.edu/sc2_public/tf_aws_account_config)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
